package soundsystem;


//@Configuration
//@ComponentScan
public class CDPlayerConfig {
	
//	@Bean(name="lonelyHeartsClubBand")
	public CompactDisc sgtPeppers() {
		return new SgtPeppers();
	}
	
//	@Bean
	public CDPlayer cdPlayer(CompactDisc compactDisc) {
		CDPlayer player = new CDPlayer();
		player.setCompactDisc(compactDisc);
		return player;
	}
	
//	@Bean
	public CDPlayer cdPlayer() {
		return new CDPlayer();
	}
	
//	@Bean
	public CDPlayer anotherCdPlayer() {
		return new CDPlayer();
	}
}