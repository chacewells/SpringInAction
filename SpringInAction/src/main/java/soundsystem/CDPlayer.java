package soundsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CDPlayer implements MediaPlayer {
  private CompactDisc cd;
  
  public void play() {
    cd.play();
  }
  
  @Autowired
  public void setCompactDisc(CompactDisc cd) {
	  this.cd = cd;
  }

}