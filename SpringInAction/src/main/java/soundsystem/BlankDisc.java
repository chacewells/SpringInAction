package soundsystem;

import java.util.List;

//@Component
public class BlankDisc implements CompactDisc {

	private String title;
	private String artist;
	private List<String> tracks;
	
//	@Autowired
	public void setTitle(String title) {
		this.title = title;
	}
	
//	@Autowired
	public void setArtist(String artist) {
		this.artist = artist;
	}

//	@Autowired
	public void setTracks(List<String> tracks) {
		this.tracks = tracks;
	}

	public void play() {
		System.out.println("Playing " + title + " by " + artist);
		for (String track : tracks) {
			System.out.println("-Track: " + track);
		}
	}

}
